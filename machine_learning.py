import csv
import re
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn import metrics
import numpy as np
import pandas as pd
from sklearn.pipeline import make_pipeline, Pipeline
from nltk.tokenize import TweetTokenizer
from sklearn import svm, linear_model
from scipy.sparse import csr_matrix
import nltk
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC
from os import path

# load in the training- test- and total dataset via csv
df_training = pd.read_csv("classifier/training_set/all_training_tweets.csv", delimiter ="," ,quotechar="|")
df_testing = pd.read_csv("classifier/test_set/all_test_tweets.csv", delimiter ="," ,quotechar="|")
df_predicting = pd.read_csv("all_tweets.csv", delimiter ="," ,quotechar="|")


text_train, y_train = df_training.tweet, df_training.label
text_test, y_test = df_testing.tweet, df_testing.label
text_total, folder_total = df_predicting.tweet, df_predicting.folder


# extract stopwords from external textfile
def get_stopwords(text_file):
    stopwords = []
    with open(text_file, "rb") as f:
        for line in f:
            line = line.strip()
            line = line.lower()
            stopwords.append(line)
    return stopwords


def identity(x):
    return x


def tokenizer(tweet):
    """tokenizes tweet, returns tokenized tweet"""
    tknzr = TweetTokenizer(reduce_len=True)
    return tknzr.tokenize(tweet)


x_train = []
for line in text_train:
    x_train.append(tokenizer(line))

x_test = []
for line in text_test:
    x_test.append(tokenizer(line))


x_total = []
for line in text_total:
    x_total.append(tokenizer(line))

x_total_folder = []
for line in folder_total:
    x_total_folder.append(line)



stopwords = get_stopwords('classifier/stopwords.txt')
# give settings for best classifier to use
vec_svm2_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords)




#best model used to classify remainder data:
print("LINEAR SVM UNIGRAMS MODEL 2")
classifier13 = Pipeline([("vec", vec_svm2_1), ("cls", svm.LinearSVC())])
classifier13.fit(x_train, y_train)
y_guess = classifier13.predict(x_total)
#print(metrics.accuracy_score(y_test, y_guess))
#print(metrics.classification_report(y_test, y_guess))

# FUNTION TO WRITE PREDICTIONS TO PARTY FOLDERS
def write_test_to_file(X, y, folders):
    """takes in list of test tweets and a list with guessed labels,
    sends tweets from list to file corresponding with label,
    e.g. if label was positive, it will be sent to the file 'positive_tweets.csv'
    used to write remainder data to positive and negative files, for later analysis of sentiment"""
    for index, item in enumerate(X):
        label = y[index]
        script_dir = path.dirname(__file__)
        if label == "pos":
            rel_path = "classifier/prediction/results/pos/positive_tweets.csv"
            abs_file_path = path.join(script_dir, rel_path)
            with open(abs_file_path, "a", newline="") as f:
                writer = csv.writer(f, delimiter=",",quotechar="|")
                writer.writerow([item]+[folders[index]])
        elif label == "neg":
            rel_path = "classifier/prediction/results/neg/negative_tweets.csv"
            abs_file_path = path.join(script_dir, rel_path)
            with open(abs_file_path, "a", newline="") as f:
                writer = csv.writer(f, delimiter=",",quotechar="|")
                writer.writerow([item]+[folders[index]])
# UNCOMMENT TO INITIATE LABELING OF TOTAL DATASET
#write_test_to_file(text_total, y_guess, x_total_folder)





#all the different tested classifiers with their settings
# remove """ to get all the results of the classifiers
test_and_training_results = """

vec_nb_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=7000)
vec_nb_12 = TfidfVectorizer(preprocessor=identity, binary=True, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=7000)
vec_nb_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, min_df=2, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=8300)


vec_1 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords)
vec_12 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords)
vec_123 = TfidfVectorizer(preprocessor=identity, binary=True, min_df=10, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords)


vec_lr_1 = TfidfVectorizer(preprocessor=identity, norm="l1", min_df=10, binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=4500)
vec_lr_12 = TfidfVectorizer(preprocessor=identity, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=4500)
vec_lr_123 = TfidfVectorizer(preprocessor=identity, norm="l1", min_df=10, binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=4500)

vec_lr2_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords, max_features=4500)
vec_lr2_12 = TfidfVectorizer(preprocessor=identity, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords, max_features=4500)
vec_lr2_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords, max_features=4500)


vec_svm_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 1), stop_words=stopwords)
vec_svm_12 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 2), stop_words=stopwords)
vec_svm_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, min_df=10, ngram_range=(1, 3), stop_words=stopwords)

vec_svm2_1 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 1), stop_words=stopwords)
vec_svm2_12 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 2), stop_words=stopwords)
vec_svm2_123 = TfidfVectorizer(preprocessor=identity, norm="l1", binary=True, tokenizer=identity, ngram_range=(1, 3), stop_words=stopwords)


print("NAIVE BAYES UNIGRAMS")
classifier1 = Pipeline([("vec", vec_nb_1), ("cls", MultinomialNB(alpha=4.0))])
classifier1.fit(x_train, y_train)
y_guess = classifier1.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("NAIVE BAYES UNIGRAMS & BIGRAMS")
classifier2 = Pipeline([("vec", vec_nb_12), ("cls", MultinomialNB(alpha=5.0))])
classifier2.fit(x_train, y_train)
y_guess = classifier2.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("NAIVE BAYES UNIGRAMS, BIGRAMS & TRIGRAMS")
classifier3 = Pipeline([("vec", vec_nb_123), ("cls", MultinomialNB(alpha=6.0))])
classifier3.fit(x_train, y_train)
y_guess = classifier3.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS")
classifier4 = Pipeline([("vec", vec_1), ("cls", BernoulliNB(alpha=6.0))])
classifier4.fit(x_train, y_train)
y_guess = classifier4.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS & BIGRAMS")
classifier5 = Pipeline([("vec", vec_12), ("cls", BernoulliNB(alpha=6.0))])
classifier5.fit(x_train, y_train)
y_guess = classifier5.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("BERNOULLI NAIVE BAYES UNIGRAMS BIGRAMS & TRIGRAMS")
classifier6 = Pipeline([("vec", vec_123), ("cls", BernoulliNB(alpha=6.0))])
classifier6.fit(x_train, y_train)
y_guess = classifier6.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS")
classifier7 = Pipeline([("vec", vec_1), ("cls", linear_model.SGDClassifier())])
classifier7.fit(x_train, y_train)
y_guess = classifier7.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS & BIGRAMS")
classifier8 = Pipeline([("vec", vec_12), ("cls", linear_model.SGDClassifier())])
classifier8.fit(x_train, y_train)
y_guess = classifier8.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM SGDC UNIGRAMS BIGRAMS & TRIGRAMS")
classifier9 = Pipeline([("vec", vec_123), ("cls", linear_model.SGDClassifier())])
classifier9.fit(x_train, y_train)
y_guess = classifier9.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS MODEL 1")
classifier10 = Pipeline([("vec", vec_svm_1), ("cls", svm.LinearSVC())])
classifier10.fit(x_train, y_train)
y_guess = classifier10.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS MODEL 1")
classifier11 = Pipeline([("vec", vec_svm_12), ("cls", svm.LinearSVC())])
classifier11.fit(x_train, y_train)
y_guess = classifier11.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS TRIGRAMS MODEL 1")
classifier12 = Pipeline([("vec", vec_svm_123), ("cls", svm.LinearSVC())])
classifier12.fit(x_train, y_train)
y_guess = classifier12.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS MODEL 2")
classifier13 = Pipeline([("vec", vec_svm2_1), ("cls", svm.LinearSVC())])
classifier13.fit(x_train, y_train)
y_guess = classifier13.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS MODEL 2")
classifier14 = Pipeline([("vec", vec_svm2_12), ("cls", svm.LinearSVC())])
classifier14.fit(x_train, y_train)
y_guess = classifier14.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LINEAR SVM UNIGRAMS BIGRAMS TRIGRAMS MODEL 2")
classifier15 = Pipeline([("vec", vec_svm2_123), ("cls", svm.LinearSVC())])
classifier15.fit(x_train, y_train)
y_guess = classifier15.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS MODEL 1:")
classifier16 = Pipeline([("vec", vec_svm_1), ("cls", svm.SVC(kernel="linear"))])
classifier16.fit(x_train, y_train)
y_guess = classifier16.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS MODEL 1:")
classifier17 = Pipeline([("vec", vec_svm_12), ("cls", svm.SVC(kernel="linear"))])
classifier17.fit(x_train, y_train)
y_guess = classifier17.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS TRIGRAMS MODEL 1:")
classifier18 = Pipeline([("vec", vec_svm_123), ("cls", svm.SVC(kernel="linear"))])
classifier18.fit(x_train, y_train)
y_guess = classifier18.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))

print("SVM LINEAR KERNEL UNIGRAMS MODEL 2:")
classifier19 = Pipeline([("vec", vec_svm2_1), ("cls", svm.SVC(kernel="linear"))])
classifier19.fit(x_train, y_train)
y_guess = classifier19.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS MODEL 2:")
classifier20 = Pipeline([("vec", vec_svm2_12), ("cls", svm.SVC(kernel="linear"))])
classifier20.fit(x_train, y_train)
y_guess = classifier20.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("SVM LINEAR KERNEL UNIGRAMS BIGRAMS TRIGRAMS MODEL 2:")
classifier21 = Pipeline([("vec", vec_svm2_123), ("cls", svm.SVC(kernel="linear"))])
classifier21.fit(x_train, y_train)
y_guess = classifier21.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS MODEL 1")
classifier22 = Pipeline([("vec", vec_lr_1), ("cls", linear_model.LogisticRegression(C=1))])
classifier22.fit(x_train, y_train)
y_guess = classifier22.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS MODEL 1")
classifier23 = Pipeline([("vec", vec_lr_12), ("cls", linear_model.LogisticRegression(C=1))])
classifier23.fit(x_train, y_train)
y_guess = classifier23.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS TRIGRAMS MODEL 1")
classifier24 = Pipeline([("vec", vec_lr_123), ("cls", linear_model.LogisticRegression(C=1))])
classifier24.fit(x_train, y_train)
y_guess = classifier24.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS MODEL 2")
classifier25 = Pipeline([("vec", vec_lr2_1), ("cls", linear_model.LogisticRegression(C=1))])
classifier25.fit(x_train, y_train)
y_guess = classifier25.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS MODEL 2")
classifier26 = Pipeline([("vec", vec_lr2_12), ("cls", linear_model.LogisticRegression(C=1))])
classifier26.fit(x_train, y_train)
y_guess = classifier26.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))


print("LOGISTIC REGRESSION UNIGRAMS BIGRAMS TRIGRAMS MODEL 2")
classifier27 = Pipeline([("vec", vec_lr2_123), ("cls", linear_model.LogisticRegression(C=1))])
classifier27.fit(x_train, y_train)
y_guess = classifier27.predict(x_test)
print(metrics.accuracy_score(y_test, y_guess))
print(metrics.classification_report(y_test, y_guess))

#shows the most informative feature per classifier
def show_most_informative_features(vectorizer, clf, n=20):
    feature_names = vectorizer.get_feature_names()
    coefs_with_features = sorted(zip(clf.coef_[0], feature_names))
    most_informative = zip(coefs_with_features[:n], coefs_with_features[:-(n + 1):-1])
    for (coef_1, fn_1), (coef_2, fn_2) in most_informative:
        print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))

print("Most informative features Linear SVM Model 2 Unigrams")
print(show_most_informative_features(vec_svm2_1, classifier13.named_steps["cls"]))

print("Most informative features Linear SVM Model 2 Unigrams")
print(show_most_informative_features(vec_svm2_1, classifier13.named_steps["cls"], n=40))

print("Most informative features NB")
print(show_most_informative_features(vec_nb_12, classifier2.named_steps["cls"]))
print(show_most_informative_features(vec_nb_123, classifier3.named_steps["cls"]))

print("Most informative features Bernoulli NB")
print(show_most_informative_features(vec_12, classifier5.named_steps["cls"]))
print(show_most_informative_features(vec_123, classifier6.named_steps["cls"]))

print("Most informative features Linear SVM")
print(show_most_informative_features(vec_svm_12, classifier11.named_steps["cls"]))
print(show_most_informative_features(vec_svm_123, classifier12.named_steps["cls"]))

print(show_most_informative_features(vec_svm2_12, classifier14.named_steps["cls"]))
print(show_most_informative_features(vec_svm2_123, classifier15.named_steps["cls"]))

print("Most informative features Logistic Regression")
print(show_most_informative_features(vec_lr_12, classifier23.named_steps["cls"]))
print(show_most_informative_features(vec_lr_123, classifier24.named_steps["cls"]))

print(show_most_informative_features(vec_lr2_12, classifier26.named_steps["cls"]))
print(show_most_informative_features(vec_lr2_123, classifier27.named_steps["cls"]))
"""


