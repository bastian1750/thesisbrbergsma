import os
import sys
import csv
import glob
from nltk.util import ngrams

def filter(location):
    """function filters tweets mentioning politcal parties, using keywords"""
    party_list = [f for f in os.listdir("{}_data/parties".format(location))]
    party_dict = {}
    with open("{}_data/alias.txt".format(location)) as txtfile:
        text = txtfile.read()
        text = text.strip()
        text = text.split("\n")
        for line in text:
            abrev = (line.split(",")[0]).strip()
            full = (line.split(",")[1]).strip()
            party_dict[abrev] = [abrev, full]
            party_dict[full] = [abrev,full]


    for filename in glob.glob('{}_data/{}_mar.csv'.format(location,location)):
        print("currently filtering: {}".format(location))
        # load in data from csv-files
        with open(filename, 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                tweet = row[2]
                tweet = tweet.strip().lower()
                tweet = tweet.split()
                bigrams = [' '.join(grams) for grams in ngrams(tweet,2)]
                trigrams = [' '.join(grams) for grams in ngrams(tweet,3)]
                fourgrams = [' '.join(grams) for grams in ngrams(tweet,4)]



                for item in party_list:
                    if item in party_dict:
                        alias_list = party_dict[item]
                    else:
                        alias_list = [item]

                    for party in alias_list:

                        # create different keywords
                        at = "@{}".format((party).replace(" ",""))
                        hashtag = "#{}".format((party).replace(" ",""))
                        at_mun = "@{}{}".format((party).replace(" ",""),location)
                        hashtag_mun = "#{}{}".format((party).replace(" ",""),location)
                        with_mun = "{}{}".format((party).replace(" ",""),location)
                        stem = "stem {}".format(party)
                        hashtag_stem = "#stem{}".format((party).replace(" ",""))
                        kies = "kies {}".format(party)
                        hashtag_kies = "#kies{}".format((party).replace(" ",""))

                        #search for keyword in unigrams, bigrams, trigrams and fourgrams
                        if party in tweet or party in bigrams or party in trigrams or  party in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif at  in tweet or at in bigrams or at in trigrams or at in fourgrams :
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif hashtag in tweet or hashtag in bigrams or hashtag in trigrams or hashtag in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif at_mun in tweet or at_mun in bigrams or at_mun in trigrams or at_mun in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif hashtag_mun in tweet or hashtag_mun in bigrams or hashtag_mun in trigrams or hashtag_mun in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif with_mun in tweet or with_mun in bigrams or with_mun in trigrams or with_mun in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif stem in tweet or stem in bigrams or stem in trigrams or stem in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif hashtag_stem in tweet or hashtag_stem in bigrams or hashtag_stem in trigrams or hashtag_stem in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif kies in tweet or kies in bigrams or kies in trigrams or kies in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)

                        elif hashtag_kies in tweet or hashtag_kies in bigrams or hashtag_kies in trigrams or hashtag_kies in fourgrams:
                            with open('{}_data/parties/{}/tweets.csv'.format(location,item), 'ab') as csvfile2:
                                spamwriter = csv.writer(csvfile2, delimiter=',', quotechar='|')
                                spamwriter.writerow(row)



def main(argv):

    for foldername in glob.glob('*_data'):
        filter(foldername.split("_")[0])

main(sys.argv)
